// MundoServidor.h: interface for the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
//añadir
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<DatosMemCompartida.h>
#include<sys/mman.h>
#include <pthread.h>
#include<Socket.h>
//

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
//añadir
#define MAX_BUFFER 1024

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
//añadir un atributo del tipo DatosMemCompartida 
	//DatosMemCompartida MemComp;
//añadir un atributo del tipo puntero a DatosMemCompartida 
	//DatosMemCompartida *Memoria;
	


	int puntos1;
	int puntos2;
//añadir
	int fd;
	char buf[MAX_BUFFER];
	//int fichbot;
	//int fb;
	//int fdsr;
	pthread_t th1;
	Socket soc_conexion;
	Socket soc_comunicacion;
	char nombre1[200];
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
