#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>

#define MAX_BUFFER 1024

int main(){
	int fd;
	char buf[MAX_BUFFER];
	mkfifo("/tmp/mififo1",0777);
	fd=open("/tmp/mififo1",O_RDWR);
	if(fd<0) perror("error al abrir el pipe logger\n");

	while(1){
		char cad[200];
		read(fd, cad,sizeof(cad));
		printf("%s\n",cad);
	if(strcmp(cad,"Fin del programa")==0) break;
	}

	close(fd);
	unlink("/tmp/mififo1");
	return 0;

}


